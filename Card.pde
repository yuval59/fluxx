abstract class Card
{
  abstract void play();
  protected Player owner;
}